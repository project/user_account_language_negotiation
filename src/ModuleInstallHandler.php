<?php

namespace Drupal\user_account_language_negotiation;

use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Language\LanguageManager;
use Drupal\locale\PoDatabaseWriter;
use Drupal\user_account_language_negotiation\Plugin\LanguageNegotiation\LanguageNegotiationUserAccountSaver;

class ModuleInstallHandler {

  const CONFIG_KEY = 'negotiation.language_interface.enabled';

  public function onInstall() {
    $this->translateLanguageNames();
  }

  public function onUninstall() {
    $this->disableOurPluginIfNeeded();
  }

  private function translateLanguageNames(): void {
    $writer = new PoDatabaseWriter();
    $writer->setOptions(['overwrite_options' => []]);
    $item = new PoItem();
    foreach (LanguageManager::getStandardLanguageList() as $langcode => $long_versions) {
      if ($long_versions[0] == $long_versions[1]) {
        // No translation needed.
        continue;
      }

      $item->setSource($long_versions[0]);
      $item->setTranslation($long_versions[1]);

      $writer->setLangcode($langcode);
      $writer->writeItem($item);
    }
  }

  /**
   * Prevent exception "The "language-user-account-saver" plugin does not exist".
   */
  private function disableOurPluginIfNeeded(): void {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('language.types');

    $enabled_plugins = $config->get(self::CONFIG_KEY);
    unset($enabled_plugins[LanguageNegotiationUserAccountSaver::METHOD_ID]);
    $config->set(self::CONFIG_KEY, $enabled_plugins);
    $config->save();
  }
}
